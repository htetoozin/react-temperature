import React, { useState } from 'react';

const App = () => {
  const [value, setValue] = useState(10);
  const [color, setColor] = useState('cold');

  const increase = () => {
    if (value >= 30) return;
    const increaseValue = value + 1;

    if (increaseValue >= 15) {
      setColor('hot');
    }
    setValue(increaseValue);
  };

  const decrease = () => {
    if (value <= 0) return;
    const decreaseValue = value - 1;

    if (decreaseValue < 15) {
      setColor('cold');
    }
    setValue(decreaseValue);
  };

  return (
    <div className='app-container'>
      <div className='temperature-display-container'>
        <div className={`temperature-display ${color}`}>{value} °C</div>
      </div>
      <div className='button-container'>
        <button onClick={() => increase()}>+</button>
        <button onClick={() => decrease()}>-</button>
      </div>
    </div>
  );
};

export default App;
